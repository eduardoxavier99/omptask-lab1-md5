# OpenMP -for- Labs

Let's learn parallel programming using OpenMP!

## MD5

The MD5 message-digest algorithm is a widely used hash function producing a 128-bit hash value.

See [Wikipedia](https://en.wikipedia.org/wiki/MD5).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor. Build the application on your computer and run it.
4. Profile the application using perf.
5. Duplicate the code and parallelize it using OpenMP tasks.
6. Compare performance between serial and parallel versions. Determine the speedup.


Anything missing? Ideas for improvements? Make a pull request.
